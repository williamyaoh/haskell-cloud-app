{ mkDerivation, aeson, base, hpack, lib, servant-server, warp, wreq
}:
mkDerivation {
  pname = "haskell-cloud-app";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  libraryToolDepends = [ hpack ];
  executableHaskellDepends = [ aeson base servant-server warp wreq ];
  prePatch = "hpack";
  license = lib.licenses.bsd3;
  mainProgram = "haskell-cloud-app-exe";
}
