provider "aws" {
  region = "us-west-2"
}

resource "aws_ecs_cluster" "haskell-cloud-app" {
  name = "haskell-cloud-app"
}

resource "aws_security_group" "haskell-cloud-app-access" {
  name = "haskell-cloud-app-access"

  ingress {
    from_port = 8000
    to_port = 8000
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "haskell-cloud-app" {
  ami = "ami-0302f3ec240b9d23c"
  instance_type = "t2.medium"
  vpc_security_group_ids = [
    aws_security_group.haskell-cloud-app-access.id
  ]

  iam_instance_profile = "ecs-instance-role"

  user_data = <<-EOF
    #!/bin/bash
    hostname ${aws_ecs_cluster.haskell-cloud-app.name}-server
    echo ECS_CLUSTER=${aws_ecs_cluster.haskell-cloud-app.name} \
      >> /etc/ecs/ecs.config
    echo ECS_ENGINE_TASK_CLEANUP_WAIT_DURATION='30m' \
      >> /etc/ecs/ecs.config
  EOF

  tags = {
    Name = "haskell-cloud-app"
  }
}

resource "aws_ecr_repository" "haskell-cloud-app" {
  name = "haskell-cloud-app"
}

resource "aws_ecs_service" "haskell-cloud-app" {
  name = "haskell-cloud-app-server"
  cluster = aws_ecs_cluster.haskell-cloud-app.id
  task_definition = aws_ecs_task_definition.haskell-cloud-app.arn
  desired_count = 1
  launch_type = "EC2"

  load_balancer {
    target_group_arn = aws_alb_target_group.haskell-cloud-app.arn
    container_port = 8000
    container_name = "haskell-cloud-app"
  }

  depends_on = [ "aws_alb_listener.http_forward" ]
}

data "template_file" "task-definition" {
  template = "${file("haskell-cloud-app-service.json")}"
  vars = {
    repository_url = aws_ecr_repository.haskell-cloud-app.repository_url
  }
}

resource "aws_ecs_task_definition" "haskell-cloud-app" {
  family = "haskell-cloud-app-service"
  container_definitions = data.template_file.task-definition.rendered
}

resource "aws_security_group" "load-balancer-access" {
  name = "load-balancer-access"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_alb" "haskell-cloud-app" {
  name = "haskell-cloud-app-lb"
  internal = false
  load_balancer_type = "application"
  security_groups = [aws_security_group.load-balancer-access.id]

  subnets = [ "<subnet1>", "<subnet2>" ]
}

resource "aws_alb_target_group" "haskell-cloud-app" {
  name = "haskell-cloud-app-tg"
  port = 8000
  protocol = "HTTP"
  deregistration_delay = 30
  target_type = "instance"

  vpc_id = "<vpc_id>"

  health_check {
    path = "/internal/health"
    timeout = 5
    unhealthy_threshold = 10
    healthy_threshold = 2
    interval = 30
  }

  depends_on = [ "aws_alb.haskell-cloud-app" ]
}

resource "aws_alb_listener" "http_forward" {
  load_balancer_arn = aws_alb.haskell-cloud-app.arn
  port = "80"
  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_alb_target_group.haskell-cloud-app.arn
  }
}

output "haskell-cloud-app-endpoint" {
  value = aws_alb.haskell-cloud-app.dns_name
}
