{ pkgs ? import <nixpkgs> {} }:

let 
  haskell-cloud-app = import ../haskell-cloud-app { inherit pkgs; };
in

with pkgs;

dockerTools.buildImage {
  name = "haskell-cloud-add-image";

  addToRoot = buildEnv { 
    name = "image-root";
    paths = [ haskell-cloud-app iana-etc cacert ];
  };

  config = {
    Cmd = [ "${haskell-cloud-app}/bin/haskell-cloud-app-exe" ];
    ExposedPorts = {
      "8000/tcp" = {};
    };
  };
}
